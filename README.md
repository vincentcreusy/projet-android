# Titre du projet
Projet Android
## Auteur(s) du projet
Vincent Creusy | Dorian Dragoni
## Auteur(s) du ReadMe
Anthony Ranuzzi | Leo Monterrat
## Résumé du projet
Cette application veut présenter les notions apprises en LP Dam en Android, via différents modules
## Description de l'application
L'application comporte un menu latéral et une page principale
Les éléments du menu renvoient vers des modules présentant des fonctions d'android :
- Bibliothèque Anko
- Élements graphiques natifs
- Des cercles magiques
- Une liste de valeur
## Impressions écrans
Accueil de l'application

![accueil](/uploads/fc1c2c54400ba9239d7ad6a13c1dea0e/accueil.png)

La page comportant les cercles magiques

![cercles](/uploads/7e82bc84552143846c560c3d2c859626/cercles.png)
## Remarque d’ensemble sur le code
L'application fonctionne convenablement, mais il y a peu de personnalisation :
- Il n'y a pas eu d'ajouts d'images
- La liste d'éléments est simple
- Pas de version paysage

Néanmoins il y a des points positifs :
- Les classes et vues sont bien nommées
- L'interface est clair et va à l'essentiel