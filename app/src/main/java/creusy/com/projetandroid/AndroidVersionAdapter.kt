package creusy.com.projetandroid

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_android_versions.view.*

class AndroidVersionAdapter(private val versions: ArrayList<AndroidVersionObject>) :
    RecyclerView.Adapter<AndroidVersionAdapter.ViewHolder>() {

    override fun getItemCount(): Int = versions.size

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AndroidVersionAdapter.ViewHolder {
        return ViewHolder(p0.inflate(R.layout.item_android_versions))
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindMyObject(versions[p1])
    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false) : View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindMyObject(androidVersionObject: AndroidVersionObject) {
            with(androidVersionObject) {
                view.version_name.text = name
                view.version_date.text = date
            }
        }
    }

}