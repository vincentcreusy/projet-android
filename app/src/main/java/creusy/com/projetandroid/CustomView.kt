package creusy.com.projetandroid

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class CustomView : View {
    companion object {
        const val DELTA = 8
    }

    private var mPaint = Paint()
    private var mCircles = ArrayList<MagicCircle>()
    private var defaultWidth = 100
    private var defaultHeight = 100

    constructor(context: Context) : super(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private fun init() {
        mPaint.color = Color.BLUE
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for (mCircle in mCircles) {
            mCircle.move()
            canvas?.drawCircle(mCircle.cx, mCircle.cy, mCircle.rad, mPaint)
        }
        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        addMagicCircle(width, height)
        defaultWidth = width
        defaultHeight = height
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        var mCircle = addMagicCircle()
        mCircle.cx = event!!.x
        mCircle.cy = event.y

        return super.onTouchEvent(event)
    }

    private fun addMagicCircle(width: Int = defaultWidth, height: Int = defaultHeight): MagicCircle {
        var mCircle = MagicCircle(width.toFloat(), height.toFloat())
        mCircle.delta = DELTA
        mCircles.add(mCircle)
        return mCircle
    }
}
