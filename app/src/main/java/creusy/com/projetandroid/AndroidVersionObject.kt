package creusy.com.projetandroid

data class AndroidVersionObject(val name: String, val date: String)