package creusy.com.projetandroid

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_adapter.*

class AdapterFragment : Fragment() {

    var versions = ArrayList<AndroidVersionObject>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        versions = getDataFromArray()

        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = AndroidVersionAdapter(versions)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_adapter, container, false)
    }

    private fun getDataFromArray(): ArrayList<AndroidVersionObject> {
        var names = resources.getStringArray(R.array.versions_names)
        var dates = resources.getStringArray(R.array.versions_dates)
        var versions = ArrayList<AndroidVersionObject>()

        names.forEachIndexed { index, name ->
            versions.add(AndroidVersionObject(name, dates[index]))
        }

        return versions
    }

}
